<?php
/*
 Template Name: Timeline
*/
?>

<?php get_header(); ?>

			<div class="content content-container">
				<div class="col" id="main-content" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
						<?php if( have_rows('event') ): ?>
							<ul class="timeline">
								<div class="event-container">
									<?php while( have_rows('event') ): the_row(); 
										// vars
										$year = get_sub_field('year');
										$description = get_sub_field('description');
									?>
									<li>
										<strong><?php echo $year; ?></strong>
										<?php echo $description; ?>
									</li>
									<?php endwhile; ?>
								</div>
							</ul>
						<?php endif; ?>
					</article>

					<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
			</div>

<?php get_footer(); ?>
