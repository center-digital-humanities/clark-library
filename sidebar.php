				<div class="nav-container">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// For Search, 404's, or other pages you want to use it on
								if (is_search()) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>About</h3> <ul>%3$s</ul>'
									));
								}
								else {
								
									// If a Collections subpage
									if (is_tree(2697) || get_field('menu_select') == "collections") {
										wp_nav_menu(array(
										   	'container' => false,
										   	'menu' => __( 'Collections', 'bonestheme' ),
										   	'menu_class' => 'collections-nav',
										   	'theme_location' => 'collections-nav',
										   	'before' => '',
										   	'after' => '',
										   	'depth' => 1,
										   	'items_wrap' => '<h3>Collections</h3> <ul>%3$s</ul>'
										));
									}
									// If a Catalogs subpage
									if (is_tree(3093) || get_field('menu_select') == "catalogs") {
										wp_nav_menu(array(
										   	'container' => false,
										   	'menu' => __( 'Catalogs', 'bonestheme' ),
										   	'menu_class' => 'catalogs-nav',
										   	'theme_location' => 'catalogs-nav',
										   	'before' => '',
										   	'after' => '',
										   	'depth' => 1,
										   	'items_wrap' => '<h3>Catalogs</h3> <ul>%3$s</ul>'
										));
									}
									// If a Visit subpage
									if (is_tree(2699) || get_field('menu_select') == "visit") {
										wp_nav_menu(array(
										   	'container' => false,
										   	'menu' => __( 'Visit', 'bonestheme' ),
										   	'menu_class' => 'visit-nav',
										   	'theme_location' => 'visit-nav',
										   	'before' => '',
										   	'after' => '',
										   	'depth' => 1,
										   	'items_wrap' => '<h3>Visit</h3> <ul>%3$s</ul>'
										));
									}
									// If a Blog subpage
									if (is_category() || is_single() || is_tree(2892) || get_field('menu_select') == "blog") {
										wp_nav_menu(array(
										   	'container' => false,
										   	'menu' => __( 'Blog', 'bonestheme' ),
										   	'menu_class' => 'blog-nav',
										   	'theme_location' => 'blog-nav',
										   	'before' => '',
										   	'after' => '',
										   	'depth' => 1,
										   	'items_wrap' => '<h3>Blog</h3> <ul>%3$s</ul>'
										));
										// Show search field
										get_search_form();
									}
									// For Search, 404's, or other pages you want to use it on
									// Replace 9999 with id of parent page
									if (is_tree(858) || is_page('contact') || is_404() || get_field('menu_select') == "about") {
										wp_nav_menu(array(
											'container' => false,
											'menu' => __( 'About', 'bonestheme' ),
											'menu_class' => 'about-nav',
											'theme_location' => 'about-nav',
											'before' => '',
											'after' => '',
											'depth' => 1,
											'items_wrap' => '<h3>About</h3> <ul>%3$s</ul>'
										));
									}
								}
							?>
						</nav>
					</div>
				</div>