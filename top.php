<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>" rel="nofollow">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" class="university-logo" />
				<h1><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.jpg" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /></h1>
			</a>
		</div>
		<nav role="navigation" aria-labelledby="main navigation" class="desktop">
			<?php wp_nav_menu(array(
				'container' => false,
				'menu' => __( 'Main Menu', 'bonestheme' ),
				'menu_class' => 'main-nav',
				'theme_location' => 'main-nav',
				'before' => '',
				'after' => '',
				'depth' => 2,
			)); ?>
		</nav>
		<?php if(get_field('enable_donation', 'option') == "enable") { ?>
		<div class="give-back">
			<?php if(get_field('link_type', 'option') == "internal") { ?>
			<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
			<?php }?>
			<?php if(get_field('link_type', 'option') == "external") { ?>
			<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
			<?php }?><span class="fas fa-heart" aria-hidden="true"></span>
			<?php the_field('button_text', 'option'); ?></a>
			<?php if(get_field('supporting_text', 'option')) { ?>
			<span class="support"><?php the_field('supporting_text', 'option'); ?></span>
			<?php }?>
		</div>
		<?php } ?>
	</header>
	<?php 
		// Do this everywhere except the homepage
		if ( ! is_front_page() ) { 
		
			// Only show hero image on a page
			if ( has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
				<div id="hero" style="background-image: url('<?=$url?>');">
					&nbsp;
				</div>
			<?php } ?>
			<?php if(get_field('google_map')) { ?>
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
				<?php $location = get_field('google_map');
					if( !empty($location) ): ?>
				<div class="map">
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
				<?php endif; ?>
			<?php } ?>
			<?php get_sidebar(); ?>
			<?php	
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>