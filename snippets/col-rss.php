<div class="col rss-col <?php the_sub_field('rss_width'); ?>">
	<h3><?php the_sub_field('rss_title'); ?></h3>
	<?php $feed_url = get_sub_field('rss_url');
		$rss_amount = get_sub_field('rss_amount_to_show');
		$rss_length = get_sub_field('description_length'); ?>
	<ul>
	<?php if(function_exists('fetch_feed')) {	
		include_once(ABSPATH . WPINC . '/feed.php');
		date_default_timezone_set('America/Los_Angeles');
		$feed = fetch_feed($feed_url);
		$feed->enable_order_by_date(false);
		$limit = $feed->get_item_quantity($rss_amount);
		$items = $feed->get_items(0, $limit);	
	}
	if ($limit == 0) echo '<p>There is nothing to show at this time. Please check back again later.</p>';
	else foreach ($items as $item) : ?>
	
		<a href="<?php echo $item->get_permalink(); ?>">
			<li>
				<h4><?php echo $item->get_title(); ?></h4>
				<?php if(get_sub_field('show_date') == "yes") { ?>
				<span class="post-date"><?php echo $item->get_date('F j, Y'); ?></span>
				<?php } ?>
				<?php if(get_sub_field('show_description') == "yes") { ?>
				<div><?php echo substr($item->get_description(), 0, $rss_length); ?>...</div>
				<?php } ?>
			</li>
		</a>
		<?php endforeach; ?>
	</ul>	
	<a class="btn" href="<?php the_sub_field('rss_view_all_link'); ?>">View All<span class="hidden"> From This Feed</span></a>
</div>