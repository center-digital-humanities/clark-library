<?php
/*
 Template Name: Blog Landing Page
*/
?>

<?php get_header(); ?>
			<div class="content content-container">
				<div class="content main">
					<div class="col" id="main-content" role="main">
						<h1><?php the_title(); ?></h1>
						<?php
						    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
						    elseif ( get_query_var('page') ) { $paged = get_query_var('page');
						    } else { $paged = 1; }
						 	$args = array( 'posts_per_page' => 10, 'paged' => $paged );
							query_posts($args); 
						?>
						<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
							<h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
							<span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
							<section class="entry-content cf">
								<?php the_post_thumbnail( 'content-width' ); ?>
								<?php the_excerpt(); ?>
								<a href="<?php the_permalink() ?>" class="btn">Read More</a>
							</section>
						</article>
						<?php endwhile; ?>					
						<?php bones_page_navi(); ?>
					</div>
				</div>
			</div>
<?php get_footer(); ?>